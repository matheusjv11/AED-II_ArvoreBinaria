import static java.lang.Math.min;

public class ArvoreBinaria {
    private boolean deBusca;
    No raiz;
    public ArvoreBinaria(boolean deBusca, No raiz){
        this.raiz = raiz;
        this.deBusca = deBusca;
    }

    public ArvoreBinaria(boolean deBusca, int valueRaiz){
        this.raiz = new No(valueRaiz);
    }

    public ArvoreBinaria(String arvore){
        arvore = arvore.substring(1,arvore.length());
        String num = "";
        while(arvore.charAt(0) != '(' && arvore.charAt(0) != ')'){
            num = num + arvore.charAt(0);
            arvore = arvore.substring(1, arvore.length());
        }
        raiz = new No(Integer.parseInt(num));
        if (arvore.charAt(0) == '(') {
            arvore = construir(arvore.substring(1,arvore.length()), raiz.esq = new No());
        }
        if (arvore.charAt(0) == '('){
            arvore = construir(arvore.substring(1, arvore.length()), raiz.dir = new No());
        }
    }

    public String construir(String arvore, No no){
        String num = "";
        while (arvore.charAt(0) != '(' && arvore.charAt(0) != ')') {
            num = num + arvore.charAt(0);
            arvore = arvore.substring(1, arvore.length());
        }
        no.valor = Integer.parseInt(num);
        if (arvore.charAt(0) == '('){
            arvore = construir(arvore.substring(1,arvore.length()), no.esq =  new No());
        }
        if (arvore.charAt(0) == '('){
            arvore = construir(arvore.substring(1, arvore.length()), no.dir = new No());
        }
        return arvore.substring(1, arvore.length());
    }

    private boolean inserir(No no, No raiz){
        if(deBusca){
            if(no.valor > raiz.valor){
                if(raiz.dir != null){
                    inserir(no, raiz.dir);
                }else{
                    raiz.dir = no;
                    return true;
                }
            }else{
                if(raiz.esq != null){
                    inserir(no, raiz.esq);
                }else{
                    raiz.esq = no;
                    return true;
                }
            }
        }else{
            int valorDir;
            int valorEsq;
            valorDir = verificarCaminho(raiz.dir);
            valorEsq = verificarCaminho(raiz.esq);
            if(valorDir < valorEsq){
                inserir(no, raiz.dir);
            }else{
                inserir(no, raiz.esq);
            }
        }
        return false;
    }

    public boolean inserir(int valor) {
        No no = new No(valor);
        return inserir(no, raiz);
    }

    public int verificarCaminho(No no){
        int valorEsq;
        int valorDir;
        if(no == null){
            return 0;
        }
        valorDir = verificarCaminho(no.dir);
        valorEsq = verificarCaminho(no.esq);
        return min(valorDir, valorEsq) + 1;
    }

    public String printInOrdem(No no){
        if(no == null){
            return "";
        }
        String dir;
        String esq;
        dir = printInOrdem(no.esq);
        esq = printInOrdem(no.dir);
        return esq + ", " + no.valor + ", " + dir;
    }

    public String printPreOrdem(No no){
        if(no == null){
            return "";
        }
        String dir;
        String esq;
        dir = printPreOrdem(no.esq);
        esq = printPreOrdem(no.dir);
        return no.valor + ", " + esq + ", " + dir;
    }

    public String printPosOrdem(No no){
        if(no == null){
            return "";
        }
        String dir;
        String esq;
        dir = printPosOrdem(no.esq);
        esq = printPosOrdem(no.dir);
        return  esq + ", " + dir + ", " + no.valor;
    }

    public static void main(String[] args) {
        ArvoreBinaria nova = new ArvoreBinaria("(10(2(1)(3))(5(7)(9))");
    }
}
