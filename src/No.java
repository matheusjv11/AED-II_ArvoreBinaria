public class No {
    int valor;
    No dir;
    No esq;

    public No(int valor){
        this.valor = valor;
        dir = null;
        esq = null;
    }

    public No(int valor, No dir, No esq){
        this.valor = valor;
        this.dir = dir;
        this.esq =esq;
    }

    public No(){
        this.valor = 0;
        this.dir = null;
        this.esq = null;
    }
}
